FROM python:3.7-alpine

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install --no-cache-dir -r requirements.txt
COPY . /app

ENTRYPOINT ["python3", "app.py"]
CMD ["--help"]
ENV PYTHONUNBUFFERED=1 \
	PYTHONDONTWRITEBYTECODE=1

