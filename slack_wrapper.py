import slack
import traceback

class Slack:
	MAX_CHANNEL_NAME_LENGTH = 21

	def __init__(self, api_token):
		self.api_token = api_token
		self.client = None

	def connect(self):
		self.client = slack.WebClient(token=self.api_token)

	def get_users_in_channel(self, channel: str) -> list:
		channel_id = self._channel_name_to_channel_id(channel)
		info = self.client.conversations_members(channel=channel_id, limit=1000)

		return info['members']

	def invite_users_to_channel(self, channel: str, users: list) -> bool:
		channel_id = self._channel_name_to_channel_id(channel)
		
		for user in users:
			try:
				self.client.conversations_invite(channel=channel_id, users=user)
			except:
				print(f'Could not invite {user}')
				traceback.print_exc()

	def create_channel(self, channel: str) -> None:
		self.client.channels_create(name=channel)

	def rename_channel(self, old_name: str, new_name: str) -> None:
		old_channel_id = self._channel_name_to_channel_id(old_name)
		self.client.conversations_rename(channel=old_channel_id, name=new_name)

	def archive_channel(self, channel: str) -> None:
		channel_id = self._channel_name_to_channel_id(channel)
		self.client.conversations_archive(channel=channel_id)

	def _channel_name_to_channel_id(self, channel: str) -> str:
		channels = self.client.conversations_list(types='private_channel,public_channel', limit=1000)['channels']

		target = [c for c in channels if c['name'] == channel][0]

		return target['id']

	def send_message(self, channel: str, message: str) -> None:
		channel_id = self._channel_name_to_channel_id(channel)

		self.client.chat_postMessage(
			channel=channel_id,
			text=message,
			mrkdwn='true',
			link_names='true',
			unfurl_links='false',
			username='Slack Private->Public Bot',
		)

	def get_archived_channel_name(self, channel: str) -> str:
		if len(channel) > (self.MAX_CHANNEL_NAME_LENGTH - 5):
			return channel[0:16] + '-arch'
		else:
			return channel + '-arch'