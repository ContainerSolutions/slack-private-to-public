import click
from slack_wrapper import Slack

@click.command()
@click.option('--token', required=True, envvar='SLACK_API_TOKEN', prompt='Legacy Slack API token')
@click.option('--channel', required=True, envvar='SLACK_OLD_CHANNEL', prompt='Name of the private channel you want to convert')
def command(token, channel):
	'''Convert private Slack channels to public.'''
	S = Slack(token)
	S.connect()

	users = S.get_users_in_channel(channel)

	# Rename & archive the current channel
	old_channel = S.get_archived_channel_name(channel)
	S.rename_channel(channel, old_channel)
	S.archive_channel(old_channel)

	# Initialize the new channel
	S.create_channel(channel)
	S.invite_users_to_channel(channel, users)
	S.send_message(channel, f'''Hello @channel,
		Welcome to the new *{channel}*.
		This channel has been moved as the channel was previously private, and not easily joinable/searchable.
		Everyone who is in the previous channel has been invited to this one.
		If you have any critical information saved in the old channel, it is still browsable (*named: {old_channel}*)
		It is recommended to take this information to a more permanent storage, such as <https://container-solutions.slab.com|the wiki>.
		<https://gitlab.com/ContainerSolutions/slack-private-to-public|I am open source! Improve me!>
		''')


if __name__ == '__main__':
	command()